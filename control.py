import paho.mqtt.client as mqtt

broker_url = "192.168.100.104" # Jetank IP
broker_port = 1883
client = mqtt.Client()
client.connect(broker_url, broker_port)


class Robot:
    OPEN_PALM = 1
    CLOSED_PALM = 0
    left_hand = 0
    right_hand = 1
    current_hand = -1
    def __init__(self):
        self.broker_url = broker_url
        self.broker_port = broker_port
        self.client = mqtt.Client()
        self.client.connect(broker_url, broker_port)

    def hold(self):
        # print("STOP")
        self.client.publish(topic="Jetank/stop", payload=0, qos=0, retain=False)

    def forward(self, speed = 0.4):
        # print("FORWARD")
        self.client.publish(topic="Jetank/forward", payload=speed, qos=0, retain=False)

    def backwards(self, speed = 0.4):
        # print("BACKWARD")
        self.client.publish(topic="Jetank/backward", payload=speed, qos=0, retain=False)

    def left(self, speed = 0.4):
        # print("LEFT")
        self.client.publish(topic="Jetank/left", payload=speed, qos=0, retain=False)

    def right(self, speed = 0.4):
        # print("RIGHT")
        self.client.publish(topic="Jetank/right", payload=speed, qos=0, retain=False)

    """ I noticed that sometimes robot keep running so this must be called before program exit """
    def master_stop(self):
        self.client.publish(topic="Jetank/stop", payload=0, qos=0, retain=False)


