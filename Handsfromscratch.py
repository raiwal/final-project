import cv2
import mediapipe as mp
import control
import gestures

robot = control.Robot()
left_hand = gestures.Gestures('Left')
right_hand = gestures.Gestures('Right')

mp_drawing = mp.solutions.drawing_utils
mp_drawing_styles = mp.solutions.drawing_styles
mp_hands = mp.solutions.hands

speed = 0
# For webcam input:
cap = cv2.VideoCapture(0)
with mp_hands.Hands(
    model_complexity=0,
        min_detection_confidence=0.5,
        min_tracking_confidence=0.5) as hands:
    while cap.isOpened():
        success, image = cap.read()
        if not success:
            print("Ignoring empty camera frame.")
            # If loading a video, use 'break' instead of 'continue'.
            continue

        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        results = hands.process(image)

    # Draw the hand annotations on the image.
        image.flags.writeable = True
        image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)

        if results.multi_hand_landmarks:
            # print(results.multi_handedness)
            for idx, hand_handedness in enumerate(results.multi_handedness):
                # print(hand_handedness.classification[0].label)
                if hand_handedness.classification[0].label == 'Left':
                    # print("Now we start to process 'Left hand'") # Actually this is right hand :-)
                    for hand_landmarks in results.multi_hand_landmarks:

                        for landmark_id, lm in enumerate(hand_landmarks.landmark):
                            h, w, c = image.shape
                            cx, cy = int(lm.x * w), int(lm.y * h)
                            """ We collect all coordinates so that we can start guessing gestures """

                            left_hand.set_coordinates(landmark_id, cx, cy)

                        speed = hand_landmarks.landmark[mp_hands.HandLandmark.INDEX_FINGER_TIP].x

                        if left_hand.fist():
                            robot.hold()
                        if left_hand.open_palm():
                            robot.forward(speed)
                        if left_hand.horns():
                            robot.backwards(speed)
                        if left_hand.to_left():
                            robot.left(speed)
                        if left_hand.to_right():
                            robot.right(speed)

            for hand_landmarks in results.multi_hand_landmarks:
                mp_drawing.draw_landmarks(
                    image,
                    hand_landmarks,
                    mp_hands.HAND_CONNECTIONS,
                    mp_drawing_styles.get_default_hand_landmarks_style(),
                    mp_drawing_styles.get_default_hand_connections_style())
    # Flip the image horizontally for a selfie-view display.
        cv2.imshow('MediaPipe Hands', cv2.flip(image, 1))
        if cv2.waitKey(5) & 0xFF == 27:
            robot.master_stop()
            break
cap.release()
