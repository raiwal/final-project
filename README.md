# Description

Control Jetank robot movement with hand gestures

# Libraries and programs

* opencv
* mediapipe
* mqtt
* mosquitto 

## OpenCV
Responsible for camera reading and image processing

## Mediapipe
used to extract hand gestures

## Jetank
Actual robot. In my case servos didn't work and that is why arm movement is not implemented in any way.

## MQTT
Because mediapipe was not available on Jetson I decided to process Hand gestures in different machine and 
then feed that information to Jetank using MQTT-protocol

control.py is responsible for publishing messages to mosquitto broker

#### Installation
If you use pycharm you can select paho-mqtt from libraries. And you can also use pip 
```commandline
pip install paho-mqtt
```

Client must be installed also on Jetank notebook. 

Execute following commands inside notebook.
```commandline
import sys
!{sys.executable} -m pip install paho-mqtt
```

### mosquitto
MQTT broker. 

To avoid network port problem broker is installed on Jetank. 
```commandline
sudo apt install mosquitto
```

## Usage
1. Download 
```
ropotti.ipynb
```
to your Jetank and execute necessary steps which are initialize camera and start MQTT

2.Download to same folder 
```
Hansfromscratch.py
gestures.py
control.py
```
and run Handsfromscratch.py

## Gestures

### Stop
!["Fist"](Fist.png)

### Forward
!["Open palm"](Open_palm.png)

### Backward
!["Backward"](Horns.png)

### Left
!["Left"](Left.png)

### Right
!["Right"](Right.png)

### Speed control
Move your hand horizontally

## Author
Rainer Waltzer (@raiwal)
