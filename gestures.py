""" mediapipe finger markings"""
WRIST = 0
THUMB_TIP = 4
THUMB_IP = 3
THUMB_MCP = 2
INDEX_FINGER_TIP = 8
INDEX_FINGER_PIP = 6
MIDDLE_FINGER_TIP = 12
MIDDLE_FINGER_PIP = 10
RING_FINGER_TIP = 16
RING_FINGER_PIP = 14
# these are different from original
PINKY_FINGER_TIP = 20   # this is different from original
PINKY_FINGER_PIP = 18   # this is different from original

# Classes for fingers. Classes used like structs
class Wrist:
    def __init__(self):
        pass

class Thumb_tip:
    def __init__(self):
        pass

class Thumb_ip:
    def __init__(self):
        pass

class Thumb_mcp:
    def __init__(self):
        pass

class Middle_finger_tip:
    def __init__(self):
        pass

class Middle_finger_pip:
    def __init__(self):
        pass

class Middle_finger_tip:
    def __init__(self):
        pass

class Ring_finger_pip:
    def __init__(self):
        pass

class Ring_finger_tip:
    def __init__(self):
        pass

class Pinky_finger_tip:
    def __init__(self):
        pass

class Pinky_finger_pip:
    def __init__(self):
        pass

class Index_finger_tip:
    def __init__(self):
        pass

class Index_finger_pip:
    def __init__(self):
        pass

class Gestures:
    wrist = Wrist()
    thumb_tip = Thumb_tip()
    thumb_mcp = Thumb_mcp()
    thumb_ip = Thumb_ip()
    middle_finger_tip = Middle_finger_tip()
    middle_finger_pip = Middle_finger_pip()
    middle_finger_tip = Middle_finger_tip()
    ring_finger_pip = Ring_finger_pip()
    ring_finger_tip = Ring_finger_tip()
    pinky_finger_tip = Pinky_finger_tip()
    pinky_finger_pip = Pinky_finger_pip()
    index_finger_tip = Index_finger_tip()
    index_finger_pip = Index_finger_pip()
    def __init__(self, handeness):
        self.wrist.x = 0
        self.wrist.y = 0

        self.thumb_tip.x = 0
        self.thumb_tip.x = 0

        self.thumb_ip.x = 0
        self.thumb_ip.x = 0

        self.thumb_mcp.x = 0
        self.thumb_mcp.x = 0

        self.index_finger_tip.x = 0
        self.index_finger_tip.y = 0

        self.index_finger_pip.x = 0
        self.index_finger_pip.y = 0

        self.middle_finger_tip.x = 0
        self.middle_finger_tip.y = 0

        self.middle_finger_pip.x = 0
        self.middle_finger_pip.y = 0

        self.middle_finger_tip.x = 0
        self.middle_finger_tip.y = 0

        self.ring_finger_tip.x = 0
        self.ring_finger_tip.y = 0

        self.ring_finger_pip.x = 0
        self.ring_finger_pip.y = 0

        self.pinky_finger_tip.x = 0
        self.pinky_finger_tip.y = 0

        self.pinky_finger_pip.x = 0
        self.pinky_finger_pip.y = 0

    def set_coordinates(self, landmark_id, cx, cy):
        if landmark_id == WRIST:
            self.wrist.x = cx
            self.wrist.y = cy
        if landmark_id == THUMB_TIP:
            self.thumb_tip.x = cx
            self.thumb_tip.y = cy
        if landmark_id == THUMB_IP:
            self.thumb_ip.x = cx
            self.thumb_ip.y = cy
        if landmark_id == THUMB_MCP:
            self.thumb_mcp.x = cx
            self.thumb_mcp.y = cy
        if landmark_id == INDEX_FINGER_TIP:
            self.index_finger_tip.x = cx
            self.index_finger_tip.y = cy
        if landmark_id == INDEX_FINGER_PIP:
            self.index_finger_pip.x = cx
            self.index_finger_pip.y = cy
        if landmark_id == MIDDLE_FINGER_TIP:
            self.middle_finger_tip.x = cx
            self.middle_finger_tip.y = cy
        if landmark_id == MIDDLE_FINGER_PIP:
            self.middle_finger_pip.x = cx
            self.middle_finger_pip.y = cy
        if landmark_id == RING_FINGER_TIP:
            self.ring_finger_tip.x = cx
            self.ring_finger_tip.y = cy
        if landmark_id == RING_FINGER_PIP:
            self.ring_finger_pip.x = cx
            self.ring_finger_pip.y = cy
        if landmark_id == PINKY_FINGER_TIP:
            self.pinky_finger_tip.x = cx
            self.pinky_finger_tip.y = cy
        if landmark_id == PINKY_FINGER_PIP:
            self.pinky_finger_pip.x = cx
            self.pinky_finger_pip.y = cy

    def fist(self):
        if (self.thumb_tip.y < self.thumb_mcp.y
            and self.index_finger_tip.y > self.index_finger_pip.y
            and self.middle_finger_tip.y > self.middle_finger_pip.y
            and self.ring_finger_tip.y > self.ring_finger_pip.y
            and self.pinky_finger_tip.y > self.ring_finger_pip.y
            and self.thumb_tip.x < self.thumb_ip.x):
            return True
        return False

    def open_palm(self):
        if (self.thumb_tip.y < self.thumb_mcp.y
            and self.index_finger_tip.y < self.index_finger_pip.y
            and self.middle_finger_tip.y < self.middle_finger_pip.y
            and self.ring_finger_tip.y < self.ring_finger_pip.y
            and self.pinky_finger_tip.y < self.ring_finger_pip.y
            and self.thumb_tip.x > self.thumb_ip.x):
            return True
        return False

    def horns(self):
        if (self.thumb_tip.y < self.thumb_mcp.y
            and self.index_finger_tip.y < self.index_finger_pip.y
            and self.middle_finger_tip.y > self.middle_finger_pip.y
            and self.ring_finger_tip.y > self.ring_finger_pip.y
            and self.pinky_finger_tip.y < self.ring_finger_pip.y
            and self.thumb_tip.x < self.thumb_ip.x):
            return True
        return False

    def to_left(self):
        """ Index finger up"""
        if (self.thumb_tip.y < self.thumb_mcp.y
            and self.index_finger_tip.y < self.index_finger_pip.y
            and self.middle_finger_tip.y > self.middle_finger_pip.y
            and self.ring_finger_tip.y > self.ring_finger_pip.y
            and self.pinky_finger_tip.y > self.ring_finger_pip.y
            and self.thumb_tip.x < self.thumb_ip.x):
            return True
        return False

    def to_right(self):
        """ pinky up"""
        if (self.thumb_tip.y < self.thumb_mcp.y
            and self.index_finger_tip.y > self.index_finger_pip.y
            and self.middle_finger_tip.y > self.middle_finger_pip.y
            and self.ring_finger_tip.y > self.ring_finger_pip.y
            and self.pinky_finger_tip.y < self.ring_finger_pip.y
            and self.thumb_tip.x < self.thumb_ip.x):
            return True
        return False

